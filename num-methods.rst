Numerical Methods
=================

Discrete-Element Methods (DEM)
------------------------------

.. youtube:: 6XuyaMCjplI

Material-Point Methods (MPM)
----------------------------

Material-point methods solve conservation equations using Eulerian finite element methods while tracking materiel composition with Lagrangian particles.
It is widely used in geodynamics :cite:`may2014ptatin,may2015scalable`, engineering mechanics :cite:`chen2002evaluation`, and computer graphics :cite:`jiang2016material`.
This approach leads to field equations for density :math:`\rho` and momentum :math:`\rho \bm u`,

.. math::
   :label: field-mass-momentum

   \begin{aligned}
   \rho_t + \nabla\cdot (\rho \bm u) = 0 \\
   (\rho \bm u)_t + \nabla\cdot \mathcal F(\bm \sigma) &= \bm b \\
   \end{aligned}

where the stress :math:`\bm \sigma`, defined at particles, is an objective function of the field quantities as well as particle properties, :math:`\mathcal F` is a projection from particles to fields (evaluated via quadrature in finite element methods), and :math:`\bm b` is any external body forces.
The field conservation system :math:numref:`field-mass-momentum` is coupled with evolution equations for particles, which are advected in the velocity field :math:`\bm u` while retaining their composition, and may accumulate elastic strain, damage, etc.

Time discretization
~~~~~~~~~~~~~~~~~~~

The field equations :math:numref:`field-mass-momentum` are typically solved using implicit or semi-implicit methods due to stiffness in low-speed experiments and nearly-incompressible materials.
Particle positions are usually updated explicitly using the computed velocity :math:`\bm u`, though this can lead to instabilities that require short time steps or other mitigations :cite:`kaus2010stabilization`.

.. note::
   This section is a placeholder to demonstrate math, and does not yet properly describe the methods.

