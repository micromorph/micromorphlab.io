Publications and Presentations
======================

* An acknowledgment of Federal support and a disclaimer must appear in the publication of any material, whether copyrighted or not, based on or developed under this project, as follows:

Acknowledgment: "The authors acknowledge support by the Department of Energy, National Nuclear Security Administration, Predictive Science Academic Alliance Program (PSAAP) under Award Number DE-NA0003962."

.. Disclaimer: "This report was prepared as an account of work sponsored by an agency of the United States Government. Neither the United States Government nor any agency thereof, nor any of their employees, makes any warranty, express or implied, or assumes any legal liability or responsibility for the accuracy, completeness, or usefulness of any information, apparatus, product, or process disclosed, or represents that its use would not infringe privately owned rights. Reference herein to any specific commercial product, process, or service by trade name, trademark, manufacturer, or otherwise does not necessarily constitute or imply its endorsement, recommendation, or favoring by the United States Government or any agency thereof. The views and opinions of authors expressed herein do not necessarily state or reflect those of the United States Government or any agency thereof."

* Recipients of DOE awards are required to submit all accepted peer-reviewed manuscripts produced with complete or partial DOE funding, to the Office of Scientific and Technical Information. Submissions are made to the DOE `E-Link <https://www.osti.gov/elink/>`_ system (after clicking the E-Link, select Financial Assistance Recipients at lower left; then select the first link for Accepted Manuscript of Journal Article, and enter the information requested). Manuscripts must be submitted no later than one year after the journal publication date and before award closeout.

An * indicates funded by PSAAP

2024
---------------------

Journal
^^^^^^^^^^^^^^^^^^^^^

* *Irwin, Z.T., Clayton, J.D., Regueiro, R.A. (2024) A large deformation multiphase continuum mechanics model for shock loading of soft porous materials. Int. J. Num. Methods Eng. `10.1002/nme.7411 <http://doi.org/10.1002/nme.7411>`_

* *Pak, RYS, Bai, X. (2024) Unified wavefront singularity characterization of three-dimensional elastodynamic time-domain half-space Green’s function under impulsive boundary and internal loads.  Proc. R. Soc. A 480: 20230515. `10.1098/rspa.2023.0515 <https://doi.org/10.1098/rspa.2023.0515>`_


2023
---------------------

Journal
^^^^^^^^^^^^^^^^^^^^^

* *Yao Wang, Yida Zhang. (2023) Effect of relative humidity on the creep rate of rock salts at low stress regime. Rock Mechanics and Rock Engineering, DOI: 10.1007/s00603-023-03518-6.

* *C. Cai, N. Vlassis, L. Magee, R. Ma, Z. Xiong, B. Bahmani, T-F Wong, Y. Wang, W.C. Sun (2023), Equivariant geometric learning for digital rock physics. Part I: Estimating formation factor and effective permeability tensors, International Journal for Multiscale Computation and Engineering, 21(5):1-24. 

* *Xiong, Z., Xiao, M., Vlassis, N. N., Sun, W. (2023). A neural kernel method for capturing multiscale high-dimensional micromorphic plasticity of materials with internal structures. Computer Methods in Applied Mechanics and Engineering, 416:116317. `10.1016/j.cma.2023.116317 <https://doi.org/10.1016/j.cma.2023.116317>`_ 

* *Vlassis, N. N., Sun, W. (2023). Denoising diffusion algorithm for inverse design of microstructures with fine-tuned nonlinear material properties. Computer Methods in Applied Mechanics and Engineering, 413:116126. `10.1016/j.cma.2023.116126 <https://doi.org/10.1016/j.cma.2023.116126>`_ 

* *Vlassis, N. N., Sun, W. (2023). Geometric deep learning for computational mechanics Part II: Graph embedding for interpretable multiscale plasticity. Computer Methods in Applied Mechanics and Engineering, 404:115768. `10.1016/j.cma.2022.115768 <https://doi.org/10.1016/j.cma.2022.115768>`_ 

* *Kammardi Arunachala, P., Abrari Vajari, S., Neuner, M., Linder, C. (2023). A multiscale phase field fracture approach based on the non-affine microsphere model for rubber-like materials, Computer Methods in Applied Mechanics and Engineering, 410:115982. `10.1016/j.cma.2022.115768 <https://doi.org/10.1016/j.cma.2022.115768>`_ 

* *Neuner, M., Abrari Vajari, S., Arunachala, P. K., Linder, C. (2023). A better understanding of the mechanics of borehole breakout utilizing a finite strain gradient-enhanced micropolar continuum model. Computers and Geotechnics, 153, 105064. `10.1016/j.compgeo.2022.105064 <https://doi.org/10.1016/j.compgeo.2022.105064>`_ 

* *Jacqueline Wentz, Alireza Doostan. (2023) GenMod: A generative modeling approach for spectral representation of PDEs with random inputs,Journal of Computational Physics, 472:111691 `10.1016/j.jcp.2022.111691 <https://doi.org/10.1016/j.jcp.2022.111691>`_

* *Jacqueline Wentz and Alireza Doostan (2023). Derivative-based SINDy (DSINDy): Addressing the challenge of discovering governing equations from noisy data. Computer Methods in Applied Mechanics and Engineering, accepted for publication. `10.48550/arXiv.2211.05918 <https://doi.org/10.48550/arXiv.2211.05918>`_

* *Mengmeng Zhang, Wenting Cai, Zhong Wang, Shaoli Fang, Runyu Zhang, Hongbing Lu, Ali E. Aliev, Anvar A. Zakhidov, Chi Huynh, Enlai Gao, Jiyoung Oh, Ji Hwan Moon, Jong Woo Park, Seon Jeong Kim \& Ray H. Baughman (2023). Mechanical energy harvesters with tensile efficiency of 17.4\% and torsional efficiency of 22.4\% based on homochirally plied carbon nanotube yarns. Journal of Natural Energy. `10.1038/s41560-022-01191-7 <https://doi.org/10.1038/s41560-022-01191-7>`_ 

Conference Presentations
^^^^^^^^^^^^^^^^^^^^^

* *September 2023, Z. Xiong, M. Xiao, N. Vlassis, W.C. Sun, A neural kernel method for capturing multiscale high-dimensional micromorphic plasticity of materials with internal structures, MMLDE, El Paso, TX.

* *September 2023, N.N. Phan, H. Cai, J.D. Clayton, W.C. Sun, Motif analysis of dense granular assemblies under shear loading via graph order embedding and causality-based feature selection, 2nd MMLDE-CSET, El Paso, TX.

* *September 2023, J. Appleton, M. Homel, H. Tufo, R.A. Regueiro, Material Point Method Simulation of Sand in Split-Hopkinson Pressure Bar Compression, 14th Annual MPM Workshop, Orono, ME.

* *July 2023, J. Wentz, A. Doostan, Derivative-Based SINDy (DSINDy): Addressing the Challenge of Discovering Governing Equations from Noisy Data, USNCCM17, Albuquerque, NM.

* *July 2023, G. Norman, J. Wentz, A. Doostan, Constrained Formulation of Deep Hidden Physics Models for PDE Discovery, USNCCM17, Albuquerque, NM.

* *July 2023, S. Abrari Vajari, M. Neuner, C. Linder, Phase field fracture modeling of concrete in generalized continua, USNCCM17, Albuquerque, NM.

* *July 2023, T. Allard, N.A. Miller, M. Neuner, R.A. Regueiro, Implementing the Tardigrade Micromorphic Framework for Simulation of a Bound-particulate Material, USNCCM17, Albuquerque, NM.

* *July 2023, J. Appleton, M. Homel, S. Povolny, H. Tufo, R.A. Regueiro, Weibull Variability, Morphology, and Fracture of Brittle Material in Explicit MPM, USNCCM17, Albuquerque, NM.

* *July 2023, B. Yan, R.A. Regueiro, Direct Numerical Simulation (DNS) of Binder-Grain Composite Materials Using Pure Discrete Element Method (DEM) Modeling, USNCCM17, Albuquerque, NM.

* *July 2023, M. Neuner, S.A. Vajari, P.K. Arunachala, R.A. Regueiro, C. Linder, A finite strain gradient-enhanced micropolar continuum approach for cohesive-frictional materials, USNCCM17, Albuquerque, NM.

* *July 2023, N. Miller, F. Shahabi, J. Bishop, R.A. Regueiro, A Micromorphic Filter for Determining Stress and Deformation from Grain-Resolving DNS, USNCCM17, Albuquerque, NM.

* *July 2023, A.C. Schmid, F. Pourahmadian, A. Doostan, Data-driven Discovery of Equations Governing Ultrasonic Wave Motion, USNCCM17, Albuquerque, NM.

* *July 2023, M. Xiao, R. Ma, W.C. Sun, Geometric learning for computational mechanics Part III. Physics-locally-constrained response surface of geometric nonlinear shells, USNCCM 17, Albuquerque, NM.

* *June 2023, M.S. Kendall, A.N. Clark, JP. Davis, P.E. Specht, N.E. Peterson, A.J. Clarke, C.T. Seagle, R.A. Regueiro, Dynamic Compression of Mock Polymer-Bonded Explosive, GSCCM Biennial Meeting 2023, Chicago, IL.

* *June 2023, N. Miller, F. Shahabi, J. Bishop, R.A. Regueiro, A Micromorphic Filter for Determining Stress and Deformation from Grain-Resolving DNS, EMI 2023, Georgia Tech, Atlanta, GA.

* *June 2023, B. Yan, R.A. Regueiro, Direct Numerical Simulation (DNS) of Binder-Grain Composite Materials Using Pure Discrete Element Method (DEM) Modeling, EMI 2023, Georgia Tech, Atlanta, GA.

* *June 2023, P.B. Javadzadeh, R. Zhang, C. Paniagua, H. Chen, Y. Ren, H. Lu,  In-Situ Surface Deformation Measurement of Unconfined Compression of F50 Sand/Kel-F Particulate Composite under Quasi-Static and High-Rate Loading, SEM 2023, Orlando, FL.

* *May 2023, B. Bahmani, H.S. Suh, W.C. Sun, High-dimensional symbolic regression via neural feature polynomials for interpretable machine learning plasticity, EMI, Atlanta.

* *March 2023, J. Brown, V. Barra, N. Beams, L. Ghaffari, K. Jansen, M. Knepley, R. Shakeri, K. Stengel, J.L. Thompson, J. Wright, J. Zhang, Performance-portable Solvers for Nonlinear Mechanics, SIAM CSE 2023, Amsterdam, Netherlands.

.. 
    Conference Papers
    ^^^^^^^^^^^^^^^^^^^^^
    * ??

2022
---------------------

Journal
^^^^^^^^^^^^^^^^^^^^^

* *Abrari Vajari S., Neuner M., Arunachala P. K., Ziccarelli A., Deierlein G., Linder C., 2022. A thermodynamically consistent finite strain phase field approach to ductile fracture considering multi-axial stress states. Computer Methods in Applied Mechanics and Engineering. 400:115467. `10.1016/j.cma.2022.115467 <https://doi.org/10.1016/j.cma.2022.115467>`_

* *Xiao, M., Sun, W. (2022). Geometric prior of multi-resolution yielding manifolds and the local closest point projection for nearly non-smooth plasticity. Computer Methods in Applied Mechanics and Engineering, 400:115469. `10.1016/j.cma.2022.115469 <https://doi.org/10.1016/j.cma.2022.115469>`_

* *Neuner, M., Regueiro, R.A., Linder, C. (2022) A Unified Finite Strain Gradient-Enhanced Micropolar Continuum Approach for Modeling Quasi-Brittle Failure of Cohesive-Frictional Materials. International Journal of Solids and Structures 254–255 111841. `10.1016/j.ijsolstr.2022.111841 <https://doi.org/10.1016/j.ijsolstr.2022.111841>`_

* *Miller, N.A., Regueiro, R.A., Shahabi, F., Bishop, J.E. (2022) A micromorphic filter for determining stress and deformation measures from direct numerical simulations of lower length scale behavior, Int. J. for Numerical Methods in Engineering, `10.1002/nme.6991 <https://doi.org/10.1002/nme.6991>`_

* *Luo, H., Hu, Z., Du, Y., Xu, T., Regueiro, R.A., Alshibli, K., Lu, H. (2022) Dynamic Uniaxial Compressive Behavior of Colorado Mason Sand under High-Strain Rates, Journal of Dynamic Behavior of Materials, 8:378-396., `10.1007/s40870-022-00338-7 <https://doi.org/10.1007/s40870-022-00338-7>`_

* *Jed Brown, Valeria Barra, Natalie Beams, Leila Ghaffari, Matthew Knepley, William Moses, Rezgar Shakeri, Karen Stengel, Jeremy L. Thompson, and Junchao Zhang (2022) Performance portable solid mechanics via matrix-free p-multigrid. `10.48550/arXiv.2204.01722 <https://doi.org/10.48550/arXiv.2204.01722>`_ .

Conference Presentations
^^^^^^^^^^^^^^^^^^^^^

* *Appleton, J.M., Povolny, S.J., Homel, M.A., Crook, C. (2022) Periodically Packed Mock-PBX Uniaxial Compressions. Material Point Method Workshop, University of Texas Austin, TX.

Reports and Theses
^^^^^^^^^^^^^^^^^^^^^

* Nathan A. Miller (2022), A Micromorphic Length-Scale Coupling Framework for the Determination of Higher-Order Constitutive Models and the Multi-Scale Simulation of Heterogeneous Materials, PhD thesis, University of Colorado Boulder. `proquest <https://www.proquest.com/dissertations-theses/micromorphic-length-scale-coupling-framework/docview/2621002489/se-2>`_ 


2021
---------------------

Journal
^^^^^^^^^^^^^^^^^^^^^

* *Samuel C. Lamont, Jason Mulderrig, Nikolaos Bouklas, and Franck J. Vernerey; Rate-Dependent Damage Mechanics of Polymer Networks with Reversible Bonds; Macromolecules; 54 (23); 2021; 10801-10813 `10.1021/acs.macromol.1c01943 <https://doi.org/10.1021/acs.macromol.1c01943>`_

* *Huiyang Luo, Huiluo Chen, Runyu Zhang, Yao Ren, Boning Zhang, Richard A. Regueiro, Khalid Alshibli, and Hongbing Lu, Constitutive behavior of granular materials under high rate of uniaxial strain loading, Chapter 4, Advances in Experimental Impact Mechanics, Ed. Bo Song, Elsevier, 2021. `10.1016/B978-0-12-823325-2.00005-4 <https://doi.org/10.1016/B978-0-12-823325-2.00005-4>`_

