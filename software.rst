Software
==========

All source code is maintained on gitlab for `Ratel and workflows <https://ratel.micromorph.org/>`_ ,
and for `GEOS-MPM <https://github.com/GEOS-DEV/GEOS/tree/feature/appleton/psaap-mpm>`_  , 
and `LAMMPS-granular <https://github.com/slamont1/lammps/>`_  ,
and  `Tardigrade <https://github.com/UCBoulder/tardigrade>`_ on github.

..
  .. list-table:: Software Capability Table
   :name: tab-software
   :header-rows: 1

   * - Name
     - Methods
     - Interface Language
     - Parallelism
     - Hardware
     - Resolved length
     - Time integrator
     - Simulated time/hour
     - Initial conditions
     - Boundary conditions
     - Parameters
     - Outputs
     - Gradients
   * - ParaEllip3d-CFD
     - DEM, FV
     - C++
     - MPI+OpenMP
     - CPU
     - 100 µm
     - Explicit
     - 1 ms
     - particle shapes, positions
     - walls (CAD?)
     - elastic parameters, friction
     - positions
     - no
   * - Tardigrade
     - FE micromorphic
     - C++
     - MPI+OpenMP
     - CPU
     - 5 mm
     - Implicit
     - 100 s
     - meshed domain, loading, temperature
     - clamped, pressure, traction, friction, contact
     - micromorphic
     - displacement, stress, temperature
     - no
   * - PETSc/libCEED (TBD)
     - FE+MPM
     - C+Julia+Rust
     - MPI+CUDA/HIP
     - CPU+GPU
     - 50 µm
     - Implicit/IMEX
     - 10 ms
     - meshed domain, material points, temperature
     - clamped, pressure, traction
     - elastic, viscous, plastic
     - material points, temperature, displacement, stress
     - semi-automated


..
  ParaEllip3d-CFD
  ---------------------

..
  Tardigrade
  ---------------------


