Team
====

Co-directors
------------

* `Jed Brown <https://jedbrown.org>`_ (exascale software and hardware), Associate Professor, CU Boulder.
* `Amy Clarke <https://metallurgy.mines.edu/project/clarke-amy/>`_ (experiments), Professor, Colorado School of Mines.
* `Alireza Doostan <https://www.colorado.edu/aerospace/alireza-doostan>`_ (V&V/UQ), Professor, CU Boulder.
* `Rich Regueiro (PI) <https://ceae.colorado.edu/~regueiro/>`_ (computational multiscale multiphysics modeling), Professor, CU Boulder.


Senior Personnel
----------------

* `Khalid Alshibli <http://alshibli.utk.edu>`_ (small-scale experiments), Professor, University of Tennessee Knoxville.
* `Alisha Clark <https://www.colorado.edu/geologicalsciences/alisha-clark>`_ (Thor materials experiments), Assistant Professor, University of Colorado Boulder.
* `Layla Freeborn <https://www.colorado.edu/crdds/layla-freeborn>`_ (software, HPC training), Associate Director, Research Computing, CU Boulder.
* `Ken Jansen <https://www.colorado.edu/aerospace/kenneth-jansen>`_ (exascale, software, in-situ viz/data), Professor, CU Boulder.
* `Shelley Knuth <https://www.colorado.edu/rc/about/knuth>`_ (software, HPC training), Director, Research Computing, CU Boulder.
* `Christian Linder <https://profiles.stanford.edu/christian-linder>`_ (multiscale fracture and failure modeling), Associate Professor, Stanford University.
* `Hongbing Lu <https://me.utdallas.edu/faculty-staff/hongbing-lu/>`_ (high strain rate experiments), Professor, University of Texas at Dallas.
* `Nathan Miller <https://www.colorado.edu/center/nsi/nathan-miller>`_ , Principal Research Associate, National Security Initiatives (NSI) Center, nathan.a.miller@colorado.edu, upscaling framework for DNS-to-micromorphic continuum, MOOSE and Tardigrade code.
* `Ron Pak <https://www.colorado.edu/ceae/ronald-y-s-pak>`_ (analytical verification), Professor, CU Boulder.
* `Fatemeh Pourahmadian <https://www.colorado.edu/ceae/fatemeh-pourahmadian>`_ (V&V, UQ), Assistant Professor, CU Boulder.
* `Scott Runnels <https://www.linkedin.com/in/scott-runnels-78b74344/>`_ Integration Manager, scott.runnels@colorado.edu, CU Boulder.
* `J-H Song <https://www.colorado.edu/faculty/song/>`_ (multiscale fracture), Associate Professor, CU Boulder.
* `Steve Waiching Sun <https://engineering.columbia.edu/faculty/steve-waiching-sun>`_ (multiscale ML), Associate Professor, Columbia University.
* `Franck Vernerey <https://www.colorado.edu/lab/vernerey/>`_ (statistical micromorphic, network theory), Professor, CU Boulder.
* `Yida Zhang <https://sites.google.com/view/yida-group/home>`_ (thermal triaxial experiments, constitutive modeling), Assistant Professor, CU Boulder.

Post-doctoral Researchers and Research Software Engineers (RSEs)
-------------------------

* `Jay M Appleton <https://www.linkedin.com/in/jay-appleton/>`_, CU Boulder, jay.appleton@colorado.edu, Postdoc Advisors: Richard A Regueiro and Henry Tufo, Departments: Civil Engineering and Computer Science, Research Areas/Interests: Development and implementation of numerical methods for exascale computing, and the direct numerical simulation of granular materials with and without binder (GEOS-MPM and GEOSX-MPM).
* Gus Becker, CU Boulder, Gus.Becker@colorado.edu, in-situ CT imaging and segmentation.
* `Fabio Di Gioacchino <https://www.linkedin.com/in/fabio-di-gioacchino-phd>`_, (exascale software, Ratel), Research Software Engineer, CU Boulder.
* `Erik Jensen <https://www.linkedin.com/in/ewjensen/>`_, Simulation Engineer, CU Boulder.
* `Matthias Neuner <https://www.researchgate.net/profile/Matthias_Neuner>`_, University of Innsbruck, matthias.neuner@uibk.ac.at, Postdoc advisor: Christian Linder, Research Area: Gradient-enhanced damage-plasticity within micromorphic continuum in MOOSE and Tardigrade, Research interests: Higher order continuum models for cohesive-frictional and granular materials.
* Yao Ren, UT Dallas, yao.ren@utdallas.edu, in-situ CT imaging, high rate (SHPB) and quasi-static experiments.
* `Jeremy L Thompson <https://jeremylt.org>`_, (exascale software, Ratel), Research Software Engineer, CU Boulder.
* `Beichuan Yan <https://www.researchgate.net/profile/Beichuan-Yan>`_ (exascale, software, DNS), Research Software Engineer, CU Boulder, beichuan.yan@colorado.edu, ParaEllip3d-CFD code, LAMMPS-granular.

Graduate students
-----------------

* `Thomas Allard <https://orcid.org/0000-0002-4871-5246>`_, CU Boulder, thomas.allard@colorado.edu, PhD student working with Prof. Rich Regueiro (Civil Engineering, Engineering Science), Research Focus: apply DNS-to-micromorphic upscaling framework in MOOSE/Tardigrade to mock HE and simpler bonded particulate materials.
* `Zach Atkins <https://www.linkedin.com/in/zratkins/>`_, CU Boulder, zach.atkins@colorado.edu, PhD student, Computer Science, Ratel development.
* Ibraheem Gharaibeh, University of Tennessee Knoxville, igharaib@vols.utk.edu, PhD student, APS and ARL experiments.
* Pooyan Javadzadeh, The University of Texas at Dallas, pooyan.javadzadeh@utdallas.edu, PhD student mechanical engineering, Quasi-static, in-situ CT imaging, high strain rate experiment Kolsky bar, experimental aspects. 
* `Melia Kendall <https://www.linkedin.com/in/melia-kendall-48622813a/>`_, CU Boulder, Melia.Kendall@colorado.edu, PhD student, Geological Sciences, Thor experiments at Sandia.
* Yazeed Kokash, CU Boulder, Yazeed.Kokash@colorado.edu, PhD student, thermal triaxial compression (TC) and constitutive modeling.
* Grant Norman, CU Boulder, grant.norman@colorado.edu, experimental and simulation UQ, PhD student, Aerospace Engineering Sciences.
* `Jacob Nuttall <https://www.colorado.edu/mse/jacob-nuttall>`_, CU Boulder, jacob.nuttall@colorado.edu, PhD student with Rich Regueiro, Materials Science and Engineering, quantifying uncertainty in experiments and simulations at various scales.
* `Jarett Poliner <https://www.linkedin.com/in/jarettpoliner/>`_, Columbia, jsp2195@columbia.edu, PhD student with Steve Sun, ML for DNS-to-micromorphic upscaled constitutive equations.
* `Abigail (Abby) Schmid <https://orcid.org/0000-0002-7685-864X>`_, CU Boulder, abigail.schmid@colorado.edu, PhD student with Dr. Fatemeh Pourahmadian and Dr. Alireza Doostan (Civil Engineering/Engineering Science).
* `Karen (Ren) Stengel <https://orcid.org/0000-0002-8764-9122>`_, CU Boulder, karen.stengel@colorado.edu, PhD student with Dr. Jed Brown (Computer Science, expected graduation 2025); working on developing exascale software. Long term interests include biophysical numerical models of excitable tissues. Has experience with molecular biology and Machine Learning for weather and climate modeling.
* `Sina Abrari Vajari <https://www.linkedin.com/in/sina-abrari-vajari/>`_, Stanford University, abrari@stanford.edu, PhD student in Civil and Environmental Engineering under supervision of Prof. Christian Linder; Research Focus: Computational fracture modeling; PSAAP contribution: Phase field fracture modeling within the micromorphic continuum in MOOSE, Multiscale micromorphic damage modeling, Gradient-enhanced damage-plasticity within micromorphic continuum in MOOSE and Tardigrade.
* Zach White, CU Boulder, zachary.white-2@colorado.edu, PhD student (Mechanical Engineering, expected graduation May 2026?) under Dr. Franck Vernerey, research area: continuum modeling of dynamic networks and soft materials, project contribution: grain-resolving DNS.
.. * Yu-Hsuan Lee, University of Colorado Boulder, yuhsuan.lee@colorado.edu, PhD student working on ParaEllip3d-CFD simulations. Supported by Center for Space and Earth Science (CSES) Student Fellow Program, Los Alamos National Laboratory.
.. * `Lindsay Harrison <https://www.linkedin.com/in/lindsay-harrison/>`_, CU Boulder, lindsay.harrison@colorado.edu, PhD student, Geological Sciences, Thor experiments at Sandia. 

Undergraduate students
----------------------

* Evan Gassiot, CU Boulder, Evan.Gassiot@colorado.edu, BS student (Computer Science, expected graduation May 2025?) working with Professor Jed Brown, project contribution: Ratel development.
* Henri Mueh, CU Boulder, henri.mueh@colorado.edu, BS student (Engineering Physics, expected graduation May 2026) working with Dr. Scott Runnels, project contribution: developing overall cost-based UQ-approach.


Team Alumni
====


Co-directors
------------

* `Henry Tufo <https://www.colorado.edu/cs/henry-tufo>`_ (exascale software and hardware), Associate Professor, CU Boulder.

Senior Personnel
----------------

* `Kester Clarke <https://metallurgy.mines.edu/project/clarke-kester/>`_ (materials experiments), Associate Professor, Colorado School of Mines; now Group Leader, LANL.
* `Dylan Perkins <https://www.colorado.edu/crdds/dylan-perkins>`_ (software, HPC training), Associate Director, Research Computing, CU Boulder; now ?.

Post-doctoral Researchers and Research Software Engineers (RSEs)
-------------------------

* Camilo Duarte-Cordon, Columbia University (now at ?), cad2244@columbia.edu, ML for DNS-to-micromorphic upscaled constitutive equations, multiscale modeling.
* Nate Peterson, Colorado School of Mines (now postdoc at LANL), nepeterson@mines.edu, Postdoc advisor: Amy Clarke, Research Area: advanced characterization of mock HE samples.
* `Nikolaos Vlassis <https://mae.rutgers.edu/nikolaos-napoleon-vlassis>`_, Columbia (now Assistant Professor at Rutgers University), nnv2102@columbia.edu, postdoc (Civil Engineering & Engineering Mechanics) with Prof. Steve Sun; research area: machine learning-based constitutive modeling, geometric deep learning for microstructure graph descriptors; PSAAP contribution: ML for DNS-to-micromorphic upscaled constitutive equations.
* Jacqui Wentz, CU Boulder (now research staff member at Sandia National Laboratories, California), jacqueline.wentz@colorado.edu, Postdoc advisor: Alireza Doostan, Department: Aerospace Engineering (PhD in Applied Mathematics), Research Areas/Interests: Uncertainty quantification using polynomial chaos expansions, compressed sensing, generative models for dimension reduction.
* `Qing (Will) Yin <http://web.stanford.edu/~qingyin/>`_, Columbia (now at Apple), qy2259@columbia.edu, postdoc advisor: Prof. Steve WaiChing Sun, ML for DNS-to-micromorphic upscaled constitutive equations, multiscale modeling, viscoplasticity.

Graduate students
-----------------

* Prajwal Kammardi Arunachala, Stanford (now postdoc at JHU), prajwalk@stanford.edu, PhD student in Civil and Environmental Engineering working with Prof. Christian Linder; Research Focus: Study of mechanical and fracture properties of rubber-like polymers; PSAAP contribution: 3D macroscale fracture using EFEM in MOOSE.
* `Ning Bian <https://www.linkedin.com/in/nbian0627/>`_, UT Dallas (now at Exponent), Ning.Bian@utdallas.edu, in-situ CT imaging, high rate (SHPB) and quasi-static experiments.
* `Summer Camerlo <https://www.linkedin.com/in/summer-camerlo/>`_, Mines (now Associate Research Scientist at Sublime Systems), scamerlo@mines.edu, Master's student working with Dr. Amy Clarke (Materials Science, graduated summer 2022), research area: characterization of mock HE, technical area: bonded grains sample preparation (F50 sand and IDOX grains; Estane and Kel-F binder).
* Guilherme Caselato Gandia, UT Dallas (still), Guilherme.CaselatoGandia@UTDallas.edu, graduate student, experiments.
* `Gustavo Felicio-Perruci <https://www.linkedin.com/in/gustavofelicioperruci/>`_, UT Dallas (still), Gustavo.FelicioPerruci@UTDallas.edu, graduate student.
* Sam Lamont, CU Boulder (now postdoc at LANL), samuel.lamont@colorado.edu, PhD student (Mechanical Engineering, expected graduation May 2024) working with Dr. Franck Vernerey, research area: continuum modeling of dynamic networks and soft materials, technical interests: theory development and multiscale modeling; emphasis on biological networks (cell mechanics) and active matter (molecular machines), project contribution: statistical interpretation of grain-resolving DNS for developing statistical micromorphic theory.
* Fenton Luong, The University of Texas at Dallas (now at ?), fxl180003@utdallas.edu, PhD student mechanical engineering, in-situ CT imaging with Digital Volume Correlation (DVC), experimental aspects. 
* `Ryan McAvoy <https://profiles.stanford.edu/ryan-mcavoy>`_, Stanford University (now at UC Berkeley), rmcavoy@stanford.edu, MS student with Christian Linder, Mechanical Engineering, multiscale micromorphic gradient-damage failure modeling.
* `Christopher Paniagua <https://www.linkedin.com/in/christopher-paniagua/>`_, UT Dallas (now at Lockheed Martin, Fort Worth, Texas), Christopher.Paniagua@UTDallas.edu, MS student, CT imaging.
* `Ben Stein <https://www.linkedin.com/in/ben-stein-a26b6836/>`_, UT Dallas (now at ?), ben.stein@utdallas.edu, PhD student with Dr. Hongbing Lu (Material Science, bioengineering, expected graduation summer 2023).
* `Andrea Tyrrell <https://www.linkedin.com/in/andrea-tyrrell-892071154/>`_, CU Boulder (now at Kiewit Engineering, CO), andrea.tyrrell@colorado.edu, MS student (Civil Engineering, graduated summer 2022), Advisor: Dr. Yida Zhang, project contribution: high-pressure thermal triaxial compression experiments on mock HE and other bonded particulate material samples, technical interests: rock mechanics and modeling of sedimentary rock (sandstone).
* `Runyu Zhang <https://www.linkedin.com/in/rpzhang/>`_, UT Dallas (now at Argonne National Laboratory), runyu.zhang2@utdallas.edu, in-situ CT imaging, high rate (SHPB) and quasi-static experiments.

Undergraduate students
----------------------

* Kyle Steinsvaag, CU Boulder, Kyle.Steinsvaag@colorado.edu, BS student (Computer Science, expected graduation May 2025) working with Dr. Scott Runnels, project contribution: writing python code elasticity verification analytical solutions to compare to DNS results.

.. * Vijay Kulkarni, UT Dallas, vnk170000@utdallas.edu, assist with in-situ CT imaging, high rate (SHPB) and quasi-static experiments.
.. * Emily Szabo, CU Boulder, Emily.Szabo@colorado.edu, laser vibrometry experiments.

