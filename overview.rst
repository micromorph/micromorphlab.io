Project Overview
================


Scientific Question
---------

Is the change in mechanical behavior (i.e., force-displacement, force-time, and failure pattern) of Plastic-Bonded eXplosive (PBX) materials related to grain-size distribution or something else?  Answering the scientific question will lead to improved understanding of the manufacturing-to-physical-behavior relationship that will allow more precise design, manufacturing, and mechanical behavior prediction of PBX materials and other plastic-bonded composite granular materials.


Hypothesis
---------

The continuum specimen-scale mechanical behavior of mock High Explosive (HE) plastic-bonded granular materials is dependent upon processing variables, such as (i) press temperature, (ii) press pressure, (iii) grain-size distribution, (iv) grain type and preparation, and/or (v) binder type and formulation, which in turn affect processed material properties such as mass density, texture, grain-binder interfacial stiffness and strength, and overall specimen stiffness, strength, and thermal expansion.  


Objective
---------

To test the hypothesis, the objective is to: predict (with quantified uncertainty) at higher fidelity (compared to classical continuum mechanics), the mechanical behavior of pressed mock HE at various strain rates, upscaling validated grain-scale Direct Numerical Simulations (DNSs) within a computational multiscale micromorphic continuum mechanics framework, and as a result correlating the variation in processing variables (press temperature only for this project) of mock HE to its continuum specimen-scale mechanical behavior.


State-of-the-Art
---------

We cannot currently change manufacturing parameters prior to pressing and track the effects through a tightly-integrated, grain-resolving experimental-computational modeling (spatially in three dimensions) framework upscaled to a micromorphic continuum-scale computational simulation accounting for grain-scale features in a computationally-more-tractable manner (than the grain-resolving DNS itself) without spatial-discretization-dependence.  Exascale computing is needed to simulate these more sophisticated micromorphic multiphysics bridged-DNS simulations.  Furthermore, Validation and Uncertainty Quantification (UQ) require multiple instances of these simulations over statistical distributions of inputs (such as grain size distribution and grain contact network connectivity, material parameter distributions, boundary condition variability), with high and low fidelity.



Overarching problem
---------

Thus, the overarching problem is to quantify variation in press temperature on mechanical behavior of compressed pristine (recycled material and temperature dependence being stretch goals of the Center) mock HE material subjected to quasi-static and high-strain-rate confined and unconfined compression, in-situ and ex-situ laboratory and synchrotron X-ray imaging and computed tomography (CT), and dynamic Kolsky bar experiments.  The mock HE is composed of a mixture of ~1 mm diameter agglomerated prills of idoxuridine (IDOX, average grain diameter ~200 micrometers) (Burch et al 2017) mixed with polymeric binder (nitroplasticized Estane) (Cady et al 2006). 


Year 5 Prediction
---------

The Year 5 prediction is to predict (with quantified uncertainty) the mechanical behavior of pressed mock HE within a computational multiscale (calibrated-and-validated-DNS-informed) micromorphic continuum mechanics framework, with the following Quantities of Interest (QoIs): (1) Platen force versus displacement curves ((Integrated Task Team) ITT-5.1 (quasi-static, confined press simulation), ITT-5.2 (quasi-static, unconfined compression), ITT-5.3 (dynamic, unconfined compression)) within an average of 20%, for which the curves have three portions: (a) elastic loading slope, (b) peak force, and (c) post peak slope; (2) Grain size distribution indices (coefficient of uniformity and coefficient of gradation within 20%, post-press in ITT-5.1); (3) Fabric (norm of error / norm of reference), grain orientations, coordination number (post-press in ITT-5.1); and (4) Image-based surface strain-map comparisons through Digital Image Correlation (DIC) (ITT-5.2, ITT-5.3) within 50%.


.. figure:: length_time_plot_large_text_DNS_micro.png



Anticipated Outcomes and Benefits of Research
---------------------------------------------

The Center research will attempt to develop higher fidelity multiscale computation
through large deformation micromorphic continuum field theories informed by DNS through the latest
ML techniques calibrated and validated against a rich experimental data set. Applying advances in
V&V/UQ, Exascale computing, and Integration/Workflows will make the applicability of such an approach
to reduce uncertainty in continuum-scale computations based on statistical distributions of
materials information at the grain-scale of plastically-bonded particulate materials a reality, which has significant influence on the success of the Stockpile Stewardship Program (e.g., High Explosive (HE) materials) and beyond.
